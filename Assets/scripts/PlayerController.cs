﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    public GameObject projectile;
    public float projectileSpeed = 5;

    private Animator anim;

    private bool playerMoving;
    private Vector2 lastMove;

    private Rigidbody2D bulletRigid;
    

	// Use this for initialization
	void Start () {

        anim = GetComponent<Animator>();

	}


	
	// Update is called once per frame
	void Update () {

        playerMoving = false;

        //gets a value between 1 and -1
        if(Input.GetAxisRaw("Horizontal") >.5f || Input.GetAxisRaw("Horizontal")<-.5f) {

            transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime,0f,0f));
            playerMoving = true;
            lastMove = new Vector2(Input.GetAxisRaw("Horizontal"), 0f);
        }

        if (Input.GetAxisRaw("Vertical") > .5f || Input.GetAxisRaw("Vertical") < -.5f)
        {
            transform.Translate(new Vector3(0f,Input.GetAxisRaw("Vertical") * moveSpeed * Time.deltaTime,0f));
            playerMoving = true;
            lastMove = new Vector2(0f, Input.GetAxisRaw("Vertical"));
        }

        anim.SetFloat("moveX", Input.GetAxisRaw("Horizontal"));
        anim.SetFloat("moveY", Input.GetAxisRaw("Vertical"));
        anim.SetBool("playerMoving", playerMoving);
        anim.SetFloat("lastMoveX", lastMove.x);
        anim.SetFloat("lastMoveY", lastMove.y);

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            FireRight();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            FireLeft();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            FireDown();
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow)) {
            FireUp();
        }

    }

    void FireRight() {

        GameObject bullet = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
        bulletRigid = bullet.GetComponent<Rigidbody2D>();
        bulletRigid.velocity = new Vector3(projectileSpeed, 0f);

    }

    void FireLeft()
    {

        GameObject bullet = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
        bulletRigid = bullet.GetComponent<Rigidbody2D>();
        bulletRigid.velocity = new Vector3(-projectileSpeed, 0f);

    }

    void FireDown()
    {

        GameObject bullet = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
        bulletRigid = bullet.GetComponent<Rigidbody2D>();
        bulletRigid.velocity = new Vector3(0f, -projectileSpeed);

    }

    void FireUp()
    {

        GameObject bullet = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
        bulletRigid = bullet.GetComponent<Rigidbody2D>();
        bulletRigid.velocity = new Vector3(0f, projectileSpeed);

    }

}
